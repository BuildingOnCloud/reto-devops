# author: Jesus Ruiz
# date: 12/05/2021
# Docker Image NGINX/Alpine with parameters

FROM nginx:1.19.10-alpine
# Copy config files
COPY nginx.conf /etc/nginx/nginx.conf
# basic auth password file
COPY .htpasswd /etc/nginx/.htpasswd
COPY nodeapp.crt /etc/ssl/certs/nodeapp.crt
COPY nodeapp.key /etc/ssl/private/nodeapp.key