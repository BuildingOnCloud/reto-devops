# author: Jesus Ruiz
# date: 12/05/2021
# Docker Image Node App Alpine based

FROM    node:16-alpine3.11
# set working directory
WORKDIR /home/node/nodeapp
# copy node app files
COPY    index.js /home/node/nodeapp
COPY    package.json /home/node/nodeapp
COPY    tests/ /home/node/nodeapp/tests
COPY    utils/ /home/node/nodeapp/utils
# install npm
RUN     npm install
# expose app port
EXPOSE  3000
# execute node app
CMD     node index.js
