# Reto-DevOps CLM
![CLM Consoltores](./img/clm.png)

Este reto fue diseñado para mostrar tus habilidades DevOps. Este repositorio contiene una aplicación simple en NodeJs.

¡¡Te damos la bienvenida al desafío de CLM Consultores!! Si estás en este proceso, es porque nos interesa que puedas ser parte de nuestro equipo.
## Fase del proceso de selección:
Antes de comenzar con el desafío, te recomendamos leer las siguientes instrucciones:
  1. Es importante que realices el reto en forma tranquila (tendrás 3 días máximo para poder enviarlo). No te preocupes sino puedes completar todas las fases, para nosotros es importante que realices lo que consideras que tienes experiencia.
  2. En caso de que cumplas con el perfil técnico del cargo, la segunda fase del proceso de selección es una entrevista técnica, en dónde validaremos tus conocimientos y podrás saber con mayor detalle las funciones asociadas al cargo y sobre el equipo del área.
  3. Si continúas avanzando con nosotros, el próximo paso es una entrevista Psicolaboral con el área de Gestión de Personas y posteriormente la coordinación del ingreso a la empresa.

Una vez completado, no olvide notificar la solución **a Carla Santiago csantiago@clmconsultores.com**

Si tienes alguna duda, puedes escribir a Carla Santiago o enviar un correo a Gestión de Personas rrhh@clmconsultores.com

¡Te deseamos mucho éxito!

## La app
![NodeJs](./img/nodejs.png)

```bash
$ git clone https://gitlab.com/clm-public/reto-devops.git
Cloning into 'reto-devops'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (3/3), done.
$ cd ./reto-devops

```
### Instalar Dependencias
```bash
$ npm install
npm WARN basicservice@1.0.0 No repository field.
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@2.1.2 (node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@2.1.2: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})

added 530 packages from 308 contributors and audited 1203947 packages in 34.589s

21 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```
### Ejecutar Test
![Jest](./img/jest.jpg)

```bash
$ npm run test

> basicservice@1.0.0 test /basic-unit-test
> jest

 PASS  tests/sum.test.js
 PASS  tests/string.test.js

Test Suites: 2 passed, 2 total
Tests:       3 passed, 3 total
Snapshots:   0 total
Time:        5.655s
Ran all test suites.
```

### Ejecutar la app
```bash
$ node index.js
Example app listening on port 3000!
```
Podrá acceder a la API localmente en el puerto `3000`.

```bash
$ curl -s localhost:3000/ | jq
{
  "msg": "ApiRest prueba"
}
$ curl -s localhost:3000/public | jq
{
  "public_token": "12837asd98a7sasd97a9sd7"
}
$ curl -s localhost:3000/private | jq
{
  "private_token": "TWFudGVuIGxhIENsYW1hIHZhcyBtdXkgYmllbgo="
}
```

**NT:** En nuestro equipo consideramos que cada aplicación debe venir bien documentada por parte del desarrollador para que el equipo de **DevOps** puede realizar los procesos de automatización de una manera mas eficaz.

## El reto comienza aquí
Tienes que hacer un **fork** de este repositorio para completar los siguientes retos en tu propia cuenta de `gitlab`. **Siéntete libre de resolver el reto que desees.** La cantidad de retos resueltos nos va a permitir valorar tus habilidades y realizar una **oferta en base a las mismas**.

1. Una vez completado, no olvide notificar la solución al **Carla Santiago (csantiago@clmconsultores.com).**
2. **La solución debe venir bien documentada, ten en cuenta que vamos a ejecutar la solución que nos envies para realizar la evaluación**
3. **Tiempo de solución 3 días**

Si tiene alguna duda, adelante, [abre un issue](https://gitlab.com/clm-public/reto-devops/issues) para hacer cualquier pregunta sobre cualquier reto.

### Reto 1. Dockerize la aplicación
![docker](./img/nodedoker.jpg)

¿Qué pasa con los contenedores? En este momento **(2021)**, los contenedores son un estándar para implementar aplicaciones **(en la nube o en sistemas locales)**. Entonces, el reto es:
1. Construir la imagen más pequeña que pueda. Escribe un buen Dockerfile :)
2. Ejecutar la app como un usuario diferente de root.

## Respuesta: 
Para este primer paso primero clone el repo, ubique los archivos y ejecute las instrucciones dadas con el aplicativo y verifique que todo funcionara localmente:
![test_local_app_1](./img/test_local_app_1.png)

Luego revise en [DockerHub](https://hub.docker.com/) la imagen docker base para nodejs:
![lookforNodeDockerImage.png](./img/lookforNodeDockerImage.png)

Adicionalmente cree un postman collection que me sirviera para probar el aplicativo posteriomente, luego eso cree un archivo Dockerfile, con las instrucciones para crear la imagen que permitiera ejecutar la aplicacion dockerizada:

![testingDockerizated_2](./img/testingDockerizated_2.png)

Posteriomente hice mas ajustes a la image docker para modificar imagen base y hacerla mas pequeña y que se permitiera ejecutar con un usuario propio y no root.

![testingDockerizated_3_NoRoot](./img/testingDockerizated_3_NoRoot.png)

### Reto 2. Docker Compose
![compose](./img/docker-compose.png)

Una vez que haya dockerizado todos los componentes de la API *(aplicación de NodeJS)*, estarás listo para crear un archivo docker-compose, en nuestro equipo solemos usarlo para levantar ambientes de desarrollo antes de empezar a escribir los pipeline. Ya que la aplicación se ejecuta sin ninguna capa para el manejo de protocolo http, añace:

1. Nginx que funcione como proxy reverso a nuesta app Nodejs
2. Asegurar el endpoint /private con auth_basic
3. Habilitar https y redireccionar todo el trafico 80 --> 443

## Respuesta: 
Para este primer paso primero fue ubicar en DockerHub una imagen de NGINX y ver sus variables de parametrizacion y como se puede configurar para proxy reverso, y tambien crear la configuracion para segurizar el /private y el direccionamiento del trafico puerto 80 al 443

Para hacer el Dockerfile divide la configuracion del nginx en su propio archivo Dockerfile y asi para la nodejs, resultando en dos archivos Dockerfile especificos que luego son centralizados en un unico Dockerfile, permitiendo modificar las configuraciones independientemente:

![dockefiles](./img/dockefiles.png)

Tambien cree los certificados digitales autofirmados para habilitar https y el token secreto de acceso de la autenticacion basica requerida.

Luego probe nuevamente los casos de usos de port forwarding, la auth basic en /private y proxy reverso, estando OK.

### Reto 3. Probar la aplicación en cualquier sistema CI/CD
![cicd](./img/cicd.jpg)

Como buen ingeniero devops, conoces las ventajas de ejecutar tareas de forma automatizada. Hay algunos sistemas de cicd que pueden usarse para que esto suceda. Elige uno, travis-ci, gitlab-ci, circleci ... lo que quieras. Danos una tubería exitosa. **Gitlab-CI** es nuestra herramienta de uso diario por lo cual obtendras puntos extras si usas gitlab.

## Respuesta: 
Para este paso cree un pipeline donde al hacer commit se disparan tres etapas dependientes, etapa de build (donde se crearia una imagen docker incorporando los cambios que se hicieron en el ultimo commit), una etapa de test (donde se ejecutar el test indicado aqui con los string) y si esta OK, finalmente se creara una imagen final tageada como latest como etapa de delivery, quedando esta imagen como la apta para ser usada en ambiente de UAT, Staging o PREPROD.
Todas la imagens creadas se almacenan en el docker registry interno del proyecto en GitLab.

Cree el archivo .gitlab-ci.yml donde esta la configuracion del pipeline, esta vez usando la misma metodologia de separar las configuraciones para mayor flexibilidad y separacion de roles:

![gitlabCICD](./img/gitlabCICD.png)

En este punto tambien cree una configuracion en la etapa de test, para cree un artefacto donde se pudiera ver el reporte del test, y aunque se ejecuta bien este archivo no tiene el cuerpo del test, la idea era poder tener como prueba este artefacto generado del testing.

![pipeline1](./img/pipeline1.png)


![pipeline2](./img/pipeline2.png)


Como mejora pienso que se puede hacer uso de secret en el token de acceso al registry interno de docker.

### Reto 4. Deploy en kubernetes
![k8s](./img/k8s.png)

Ya que eres una máquina creando contenedores, ahora queremos ver tu experiencia en k8s. Use un sistema kubernetes para implementar la API. Recomendamos que uses herramientas como Minikube o Microk8s.

Escriba el archivo de implementación (archivo yaml) utilizalo para implementar su API (aplicación Nodejs).

* añade **Horizontal Pod Autoscaler** a la app NodeJS

## Respuesta: 
En esta parte del reto usando minikube me dispuse a crear un solo archivo de deployment donde estuvieran las configuraciones de namespace (despues me di cuenta que no valia la pena esto, por lo pequeño del proyecto), secret, deployment y service.

![kubernetes_deployment](./img/kubernetes_deployment.png)

Tuve que configurar un secret usando un access token para poder acceder al docker registry interno del proyecto en gitlab, porque mi idea es bajar la imagen latest producto de la ejecución del pipeline y trabajar el cluster kubernetes con esta imagen.

Ejecute el cluster para probar que funcionara todo, y poder incluir el HPA:

![HPA](./img/HPA.png)

Mas sin embargo al intentar estresar el cluster ejecutando via postman collection unas 3000 peticiones, no lo logre que escalara los pods, esto tengo que investigarlo, tal vez no lo hice de forma correcta.

### Reto 5. Construir Chart en helm y manejar trafico http(s)
![helm](./img/helm-logo-1.jpg)

Realmente el pan de cada día es crear, modificar y usar charts de helm. Este reto consiste en:

1. Diseñar un chart de helm con nginx que funcione como proxy reverso a nuesta app Nodejs
2. Asegurar el endpoint /private con auth_basic
3. Habilitar https y redireccionar todo el trafico 80 --> 443

## Respuesta: 
En este paso me dispuse a ubicar en Artifact Hub las imagenes base de nodejs y nginx, las incorpore en un template del archivo Chart.yaml:

![helm](./img/helm.png)

Lamentablemente por un tema de tiempo y que es primera vez que trabajo con Helm, no pude avanzar mas. 

En conclusion queda al pendiente:
1) HPA y por que no escala
2) Terminar la configuracion de Helm.
3) Paso 6 y 7

Debo decir que el reto fue divertido y de un nivel necesario para que fuera interesado, lamento no tener mas tiempo para terminarlo por completo, pero fue muy educativo en varios aspectos.

Estoy a la orden en: jesus@buildingoncloud.com

Gracias por la oportunidad!
